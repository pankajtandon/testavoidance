package com.nayidisha.testavoidance.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.inject.Inject;

@SpringBootTest
public class AggressiveServiceTests {

    @Inject
    private AggressiveService aggressiveService;


    @Test
    public void testAttack() {
        aggressiveService.attack();
    }

    @Test
    public void testAnnihiliate() {
        aggressiveService.annihilate();
    }
}
