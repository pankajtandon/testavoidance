package com.nayidisha.testavoidance.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.inject.Inject;

@SpringBootTest
public class HumbleServiceTests {


    @Inject
    private HumbleService humbleService;

    @Test
    public void testBow() {
        humbleService.bow();
    }

    @Test
    public void testProstrate() {
        humbleService.prostrate();
    }

}
