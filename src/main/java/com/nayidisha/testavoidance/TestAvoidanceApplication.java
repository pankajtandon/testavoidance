package com.nayidisha.testavoidance;

import lombok.extern.log4j.Log4j2;
import org.jacoco.core.analysis.Analyzer;
import org.jacoco.core.analysis.CoverageBuilder;
import org.jacoco.core.analysis.IBundleCoverage;
import org.jacoco.core.analysis.IClassCoverage;
import org.jacoco.core.analysis.IPackageCoverage;
import org.jacoco.core.tools.ExecFileLoader;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

@Log4j2
@SpringBootApplication
public class TestAvoidanceApplication implements CommandLineRunner {

    private static String EXEC_DATA_FILE = "jacoco.exec";

    @Value("classpath:jacoco.exec")
    Resource resource;

    public static void main(String[] args) {
        SpringApplication.run(TestAvoidanceApplication.class, args);
    }


    @Override
    public void run(String... args) throws  Exception {
        log.info("Processing " + EXEC_DATA_FILE);

        final ExecFileLoader loader = new ExecFileLoader();
        final Map<String, IClassCoverage> classes = new HashMap<String, IClassCoverage>();

        File execFile = resource.getFile();
        loader.load(execFile);

        File classesDirectory = new File("target", "classes");
        final CoverageBuilder coverageBuilder = new CoverageBuilder();
        final Analyzer analyzer = new Analyzer(
                loader.getExecutionDataStore(), coverageBuilder);

        analyzer.analyzeAll(classesDirectory);

        IBundleCoverage bundleCoverage= coverageBuilder.getBundle("TestAvoidanceBundleTitle");

        for (IPackageCoverage packageCoverage: bundleCoverage.getPackages()) {
            // Do processing here by checking what packages are visited etc.
            // This needs refined but can easily be done.
            if (packageCoverage.getMethodCounter().getCoveredCount() >= 1) {
                log.info(packageCoverage.getClasses() + "was covered by a test");
            } else {
                log.info(packageCoverage.getClasses() + "was not covered by a test");
            }
        }

        // Bigger issue:
        // TODO: How to determine what test caused initiated the coverage.

    }

}
