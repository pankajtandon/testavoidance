package com.nayidisha.testavoidance.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class HumbleService {

    public void bow() {
        log.info("Taking a bow!");
    }

    public void prostrate() {
        log.info("Prostrating now!");
    }

    public void waitAndWatch() {
        log.info("Waiting and watching!");
    }

    public void alertAndAttentive() {
        log.info("I've got my eye on you!");
    }
}
