package com.nayidisha.testavoidance.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class DrivenSnowService {

    public void neverTouched() {
        log.info("I've never been touched by a test!");
    }
}
