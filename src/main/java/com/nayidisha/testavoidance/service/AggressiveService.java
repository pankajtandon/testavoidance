package com.nayidisha.testavoidance.service;


import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class AggressiveService {

    public void attack() {
        log.info("Attacking!");
    }

    public void annihilate() {
        log.info("Annihiliating!");
    }

    public void rout() {
        log.info("Routing out!");
    }

    public void oppress() {
        log.info("Oppressing!");
    }
}
