Test Avoidance
===

Project to determine what tests have already been run based
on changed classes (since the last run), not run those tests again.


### To Build

```
mvn clean install
```` 

### To run

```
java -jar target/testavoidance-0.0.1-SNAPSHOT.jar
```